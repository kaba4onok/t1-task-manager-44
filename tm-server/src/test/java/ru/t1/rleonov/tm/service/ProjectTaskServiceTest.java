package ru.t1.rleonov.tm.service;

import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.marker.UnitCategory;
//import ru.t1.rleonov.tm.repository.ProjectRepository;
//import ru.t1.rleonov.tm.repository.TaskRepository;


@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    /*@NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @Before
    public void setUp() {
        projectRepository.set(ALL_PROJECTS);
        taskRepository.set(ALL_TASKS);
    }

    @After
    public void reset() {
        projectRepository.clear();
        taskRepository.clear();
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final String userId = USER2_TASK1.getUserId();
        @NotNull final String projectId = USER2_PROJECT2.getId();
        @NotNull final String taskId = USER2_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(userId, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(userId, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(userId, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(userId, projectId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.bindTaskToProject(userId, "0", taskId));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                service.bindTaskToProject(userId, projectId, "0"));
        service.bindTaskToProject(userId, projectId, taskId);
        Assert.assertEquals(USER2_TASK1, taskRepository.findAllByProjectId(userId, projectId).get(0));
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String projectId = USER1_PROJECT1.getId();
        @NotNull final String taskId = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(userId, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(userId, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(userId, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(userId, projectId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.unbindTaskFromProject(userId, "0", taskId));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                service.unbindTaskFromProject(userId, projectId, "0"));
        service.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertEquals(Collections.EMPTY_LIST, taskRepository.findAllByProjectId(userId, projectId));
    }

    @Test
    public void removeProjectById() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String projectId = USER1_PROJECT1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(null, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById("", projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(userId, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(userId, ""));
        service.removeProjectById(userId, projectId);
        Assert.assertEquals(Collections.EMPTY_LIST, taskRepository.findAllByProjectId(userId, projectId));
    }*/

}
