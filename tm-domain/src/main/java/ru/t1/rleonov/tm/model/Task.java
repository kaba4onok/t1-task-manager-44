package ru.t1.rleonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm.task")
public final class Task extends AbstractUserOwnedModel {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public Task(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public Task(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

}
